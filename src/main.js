import Amplify from 'aws-amplify';
import aws_exports from './aws-exports';
import {
  applyPolyfills,
  defineCustomElements,
} from '@aws-amplify/ui-components/loader';

import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Amplify.configure(aws_exports);
applyPolyfills().then(() => {
  defineCustomElements(window);
});

const {VUE_APP_AWS_ACCESS_ID, VUE_APP_AWS_SECRET_KEY, VUE_APP_AWS_REGION} = process.env;
var AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: VUE_APP_AWS_ACCESS_ID,
  secretAccessKey: VUE_APP_AWS_SECRET_KEY,
  region: VUE_APP_AWS_REGION
});

let rekognition = new AWS.Rekognition({});
let dynamodb = new AWS.DynamoDB({});
window.rekognition = rekognition
window.dynamodb = dynamodb

Vue.config.ignoredElements = [/amplify-\w*/];

new Vue({
  render: h => h(App),
}).$mount('#app')
